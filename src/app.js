'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');

const app = express();
const router = express.Router();

//Conecta ao banco
mongoose.connect(config.connectionString);

//Carrega Models
const Todo = require('./models/todo');

//Carrega Rotas
const indexRoute = require('./routes/index-route');
const todoRoute = require('./routes/todo-route');


app.use(bodyParser.json({
	limit: '5mb'
}));
app.use(bodyParser.urlencoded({extended: false}));

//Habilita o CORS
app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin',  '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
	next();
});

//Rotas
app.use('/about', indexRoute);
app.use('/', todoRoute);

module.exports = app;