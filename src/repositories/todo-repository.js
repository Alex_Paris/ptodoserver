'use strict'

const mongoose = require('mongoose');
const Todo = mongoose.model('Todo');

//GETs
exports.get = async() => {
	const res = await Todo.find();
	return res;
};

exports.getById = async(id) => {
	const res = await Todo.findById(id);
	return res;
};

//CRUD
exports.create = async(data) => {
	var todo = new Todo(data);
	await todo.save();
};

exports.update = async(data) => {
	await Todo
		.findByIdAndUpdate(data._id, {
			$set: {
				checked: data.checked
			}
		});
};

exports.delete = async(id) => {
	await Todo.findByIdAndRemove(id);
}