'use strict'

const repository = require('../repositories/todo-repository');


//GETs
exports.get = async(req, res, next) => {
	try {
		var data = await repository.get();
		res.status(200).send(data);
	} catch (e) {
		res.status(500).send({
			message: 'Falha ao processar sua requisição'
		});
	}
};

exports.getById = async(req, res, next) => {
	try {
		var data = await repository.getById(req.params.id);
		res.status(200).send(data);
	} catch (e) {
		res.status(500).send({
			message: 'Falha ao processar sua requisição'
		});
	}
};

//CRUD
exports.post = async(req, res, next) => {
	try {
		await repository.create({
			description: req.body.description
		});
		res.status(200).send({
			message: 'ToDo Incluído!'
		});
	} catch (e) {
		res.status(500).send({
			message: 'Falha ao processar sua requisição'
		});
	}
};

exports.put = async(req, res, next) => {
	try {
		await repository.update(req.body);
		res.status(200).send({
			message: 'ToDo Alterado'
		});
	} catch (e) {
		res.status(500).send({
			message: 'Falha ao processar sua requisição'
		});
	}
};

exports.delete = async(req, res, next) => {
	try {
		await repository.delete(req.params.id);
		res.status(200).send({
			message: 'ToDo Removido'
		});
	} catch (e) {
		res.status(500).send({
			message: 'Falha ao processar sua requisição'
		});
	}
};