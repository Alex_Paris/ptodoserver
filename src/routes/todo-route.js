'use strict'

const express = require('express');
const { route } = require('../app');
const router = express.Router();
const controller = require('../controllers/todo-controller');

router.get('/', controller.get);
router.get('/:id', controller.getById);
router.post('/', controller.post);
router.put('/', controller.put);
router.delete('/:id', controller.delete);

module.exports = router;